# Static web site

This site has been built from a markdown page and convert to html using `pandoc`.

## Purpose

This simple static web site is used as a mock http interface: It could be a web page, or an API REST.

The aim is to deploy this html page using differents technologies such as:

* On-premise deployment:
  * [Vagrant](https://www.vagrantup.com/) to create VMs on [Virtual Box](https://www.virtualbox.org/)
  * [Ansible](https://docs.ansible.com/ansible/latest/index.html) to set a minimal configuration on hosts

* IaaS:
  * [Kubernetes](https://kubernetes.io/) as container orchestrator
  * [Minikube](https://minikube.sigs.k8s.io/docs/start/) to mock a k8s cluster
  * [Helm](https://helm.sh/) for managing k8s artifacts

* PaaS:
  * [AWS](https://aws.amazon.com/): _Coming soon_


* Middlewares:
  * [Apache](https://httpd.apache.org/) as web server to host this page
  * [nginx](https://www.nginx.com/) as web server
  * [Kong Gateway](https://docs.konghq.com/gateway/latest/) as reverse proxy
  * [Docker and docker-compose](https://docs.docker.com/) to containerize apps and middlewares