# Wiki Project

## Purpose

The purpose of this project is too push a web site containing wiki pages from my personal wiki to AWS plateform (using free tier).

## Steps

### Infra

* [ ] Diagram of the infra: VM, Amazon, machine Image, Load balancer...
* [ ] AWS infra generation [using terraform](https://learn.hashicorp.com/collections/terraform/aws-get-started)
* [ ] Create a dedicated pipeline with gitlab CI:
    - Up the AWS infra
    - Test connection > `ping`, `curl` ...

### App

* [ ] Define a set a of wiki pages > docker and linux for example
* [ ] Dockerized web site: Ghost, nginx, apache ?
* [ ] Access through HTTP
    * [ ] Best effort: push a self signed certificate for HTTPS connection
* [ ] Create a pipeline that:
    - Copy pages: md to html converter
    - Create a docker image
    - basic health check > `curl`, http response ...
    - Push to AWS infra

### Monitoring

* [ ] Send web server log s3 and elasticsearch database
    * Logstash 
        -> s3
        -> elastic
        -> AWS managed Database (NoSQL)
* [ ] Display in basic Kibana dashboard
